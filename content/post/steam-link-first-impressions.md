+++
date = "2015-10-18T22:18:05-08:00"
draft = false
title = "First Impressions: The Steam Link"
slug = "first-impressions-steam-link"
aliases = ["/first-impressions-steam-link"]
+++

The pre-orders for Valve's hotly anticipated Steam Link are finally starting to arrive, and being one of the recipients of the first shipments, I thought I'd give some impressions.

## What it Does
<iframe width="560" height="315" src="https://www.youtube.com/embed/mraRO_BNQG4" frameborder="0" allowfullscreen></iframe>


The [Steam Link](http://store.steampowered.com/universe/link/) is Valve's solution to stream games from your gaming PC to you living room. The idea is that many PC gamers have a great machine, but it's likely stuck in a bedroom or an office, making it difficult to play from a couch or enjoy local multiplayer games. The Steam Link solves these problems by acting as a middle-man between your gaming PC and your living room TV. Your PC runs the games and streams the video feed to the Steam Link which is attached to your TV. You hookup input devices (like controllers, or even a mouse and keyboard) to the Steam Link, and it'll send your input commands back to your PC. The end effect is that you're playing games in your living room as if your PC was directly hooked up to your TV.

## Streaming Performance
So, how well does it work? Really well. In my few hours of playing titles like The Binding of Isaac, Broforce, and Left 4 Dead 2, I experienced no noticeable lag of any kind. The only indication I've had that this is actually streaming video is some strange artifacts that occur while displaying gradients (similar to artifacts you may see on YouTube), most notably the background of the game selection screen when you launch a game. There *is* an option in the settings to increase streaming quality from "balanced" to "beautiful", but considering I don't notice any artifacts in-game, I'd rather not risk introducing lag to fix such a minor issue.

It's worth mentioning that both my desktop PC and the Steam Link are connected to my router via ethernet. I've done Steam streaming in the past over Wi-Fi with mixed results, so I'd highly recommend you connect via ethernet if possible.


## Controller Support
On top of that, the Steam Link's controller support is quite impressive. Of course it works with the Steam Controller (which I plan to write about in another post), but it also works out-of-the-box with Xbox One controllers, Xbox 360 wired controllers, and even the now-discontinued Xbox 360 Wireless Controller Receiver! Considering I already invested in all of these devices, I was quite pleased to find out they worked with the Steam Link without any additional setup required.

As far as ports are concerned, you've got power, ethernet, HDMI, and 3 USB ports. Not bad, but it seems funny to me to only have 3 ports, since that means you'll need to pick up a USB hub if you want to have 4 wired controllers hooked up to it.

## The User Interface
![Big Picture Mode - Game Library](/img/big-picture-game-library.png)
Steam's TV user-interface, dubbed "Big Picture Mode," is quite nice. It makes it easy to access all of your various titles and settings, and there's helpful indicators as you hover over each game telling you whether or not it's controller-friendly. Overall, it does a really solid job of allowing you to navigate and manage your library without using a mouse and keyboard.

### The Web Browser
I'll be brief: Big Picture mode has a web browser, and you really shouldn't use it. Navigation is super confusing, it's hard to get zoom levels correct, the overall browsing experience is just plain awkward, even with the Steam Controller's trackpads. Do yourself a favor and spend $35 to get a [Chromecast](https://store.google.com/product/chromecast_2015) if you want to play internet videos on your TV.

### The Keyboard
The new software keyboard is pretty neat. Previously, Valve wooed us with it's "daisy wheel" keyboard, which was designed for controllers with joysticks.

![Daisy Wheel Keyboard](/img/big-picture-daisy-wheel.png)
![Standard Keyboard](/img/big-picture-standard-keyboard.png)

Now Valve has created this new, more traditional keyboard. If you're just using a regular joystick-based controller, then it operates like pretty much every other software keyboard you've ever used on a game console. However, if you have the Steam Controller, you can move two cursors separately over the keyboard at the same time. Clicking the touch pad will then select the letter underneath the cursor. The keyboard definitely stretches your brain in a weird way, but when you get used to it, you can type pretty quickly. The only issue I've run into is that when you click the touch pad, the cursor has a tendency to move, causing you to click the wrong letter. It's not too bad though, and overall I find the keyboard allows me to type quickly on the Steam Controller. However, if you're using a joystick-based controller, I'd recommend you switch to the daisy wheel keyboard in the settings.

## Closing Thoughts
Overall, the Steam Link is really great. I spend way too much time in my room playing games, leaving myself unable to take advantage of my awesome living room furniture and larger TV. The Steam Link will let me play games on my couch, with no lag, and no crazy wires. I'd highly recommend it to people who have the setup to support it.
