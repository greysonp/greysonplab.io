+++
date = "2015-10-04T18:37:00-08:00"
draft = false
title = "Adding Comments to Your Ghost Blog with Disqus"
slug = "adding-disqus-to-your-ghost-blog"
aliases = ["/adding-disqus-to-your-ghost-blog"]
+++

While setting up this very blog, one of the first things I wanted to add was commenting. By far, the easiest way to add comments to any site is [Disqus](disqus.com). If you haven't used Disqus before, it's basically a way add comments to your site without writing any server-side code. You just add some HTML and JavaScript to your site and bam: page-specific comments. Not only is it free, but it's actually quite good! It's got upvoting, multiple ways to login, moderation, etc. The only real downside is that it's near-impossible to style it to fit in with your site's theme. So it goes.

Anyway, there's some other guides out there that show you how to add Disqus to Ghost, but unfortunately they're all out-of-date. So, here we go!

First off, I'm assuming you're using [Casper](https://github.com/TryGhost/Casper), Ghost's default theme. If you're making your own theme, you can follow the ideas here, but I'm guessing you already know what you're doing at that point.

## Adding comments to the bottom of your posts.

Before you being, create a [Disqus](disqus.com) account and add a new site. Disqus is quite user-friendly, so I won't detail that process here.

Once you've done that, go into your Ghost directory. If you're using the [DigitalOcean](digitalocean.com) one-click Ghost install like I am, that's at ```/var/www/ghost```. Next, open up the file ```content/themes/casper/post.hbs```. This is the template file used to render every post in your blog. We're going to add in our comments allll the way at the bottom, right before the ```{{/post}}```. This will position the comments at the very bottom of your post, underneath the next/previous story links. Simply paste the following above ```{{/post}}```.

```handlebars
{{!-- Disqus --}}
<div id="disqus_thread" class="post"></div>
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES * * */
    var disqus_shortname = '<YOUR SHORT NAME>';

    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
```

You can get that code snippet we're adding from Disqus by going to ```https://<your short name>.disqus.com/admin/settings/universalcode/```. The main difference is that I added ```class="post"``` to the div. Now I'm aware that this isn't semantically correct, but it's pretty much the quickest way to size the comments section appropriately so it's the same width as the post content. Again, if you're making your own theme, you'd do this the right way.

To see your new comments, restart Ghost, and refresh the page! If you're using the DigitalOcean install, you can do that by running:

```bash
service Ghost restart
```

The cool part is that you don't need to do anything special to have post-specific comments. Disqus will use your post URL to uniquely identify a thread, and it will start new threads as appropriate. Very handy! You can stop here if you'd like, but we can also add one more cool thing quite easily.

## Adding comment counts to your post listings.
When looking at the list of posts on your blog, it's nice to see how many comments there are for that specific post. That's what we'll add here. To start, open the file ```content/themes/casper/partials/loop.hbs```. This is the template used to render your post listing.

In this file, you'll see the line:

```html
<time class="post-date" ...>...</time>
```

We're going to add this snippet right under that, so our comment counts will be displayed right next to the publish date of the post.

```handlebars
<a href="{{url}}#disqus_thread" class="post-date">Comments</a>
```

You can get that snippet from the same place in your Disqus settings that I mentioned earlier. However, in this case, I added ```class="post-date"```. Again, I'm aware this isn't semantically correct, but it gives us a nice left border and the same font as the date with no effort required on our part.

Just one more step. Open up ```content/themes/casper/default.hbs```. This is the big-daddy template that serves as the foundation for every page on your blog. We need to add a little snippet so Disqus finds those links we just added above and inserts the comment counts. Right before the ending ```</body>``` tag, paste:

```javascript
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES * * */
    var disqus_shortname = '<YOUR SHORT NAME>';

    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function () {
        var s = document.createElement('script'); s.async = true;
        s.type = 'text/javascript';
        s.src = '//' + disqus_shortname + '.disqus.com/count.js';
        (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
    }());
</script>
```

Finally, restart Ghost again:

```bash
service ghost restart
```

And off you go!

P.S. In my experience, it took a couple minutes before comment counts updated on my post listing, so don't fret if you don't see the correct count right away.
